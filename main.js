langs = ["en","ru"];
labels = ["main_heading",
         "calc_button",
         "tab_ratio",
          "tab_ch",
          "chainstay",
          "pitch",
          "ch_ratio",
          "fr_chainring",
          "ch_d",
          "chain_straight",
          "chain_length",
          "chain_links",
         ];

for (var k in labels) labels[labels[k]] = new Array();

labels["main_heading"]["ru"]="Расчет звезд для SingeSpeed (для установки без натяжителя)";
labels["main_heading"]["en"]="SingleSpeed (fixed) chain length calculator";
labels["calc_button"]["ru"]="вычислить";
labels["calc_button"]["en"]="calculate";
labels["tab_ratio"]["ru"] = "По отношению";
labels["tab_ratio"]["en"] = "By ratio";
labels["tab_ch"]["ru"] = "По передней зведе";
labels["tab_ch"]["en"] = "By chainring";
labels["chainstay"]["ru"] = "Длинна между осями каретки и колеса (мм):";
labels["chainstay"]["en"] = "Chainstay length:";
labels["pitch"]["ru"] = "Шаг цепи (мм):";
labels["pitch"]["en"] = "Pitch:";
labels["ch_ratio"]["ru"] = "Передаточное отношение:";
labels["ch_ratio"]["en"] = "Chainrings ratio:";
labels["fr_chainring"]["ru"] = "Передняя звезда (зубов):";
labels["fr_chainring"]["en"] = "Font chainring (number of teeth):";
labels["ch_d"]["ru"] = "Диаметры звезд:";
labels["ch_d"]["en"] = "Chainrings diameter:";
labels["chain_straight"]["ru"] = "Длинна прямых участков цепи:";
labels["chain_straight"]["en"] = "Straight chain parts length:";
labels["chain_length"]["ru"] = "Общая длинна цепи:";
labels["chain_length"]["en"] = "Full chain length:";
labels["chain_links"]["ru"] = "Количество звеньев цепи:";
labels["chain_links"]["en"] = "Number of chain links:";

/*
labels[""]["ru"] = "";
labels[""]["en"] = "";
*/

// ========================================================================
// Events
// ========================================================================

  $( "#calculate" ).click(function() { clclt('ratio'); ntnl($('#curr_lang').text()); });
  $( "#calculate_ch" ).click(function() { clclt('fr_ring'); ntnl($('#curr_lang').text()); });

  // Submit by Enter key
  $(document).keypress(function(event) {
      if (event.which == 13) {
          event.preventDefault();
          //$( "#calculate" ).click();
          clclt('ratio');
          //$( "#calculate_ch" ).click();
          clclt('fr_ring');
      }
  });

  // Reset on edit
  $("#chainstay").on("input", function()
  {
    $("#result").html('');
    $("#result_ch").html('');
  });
  $("#ratio").on("input", function() { $("#result").html(''); });
  $("#front_chring").on("input", function() { $("#result_ch").html(''); });
  
  // Lang set
  var userLang = navigator.language || navigator.userLanguage;
  if($.inArray(userLang, langs) >= 0 && $("#curr_lang").text().toLowerCase() !== userLang) ch_lang();
  var savedLang = $.cookie("clang");
  if( $("#curr_lang").text().toLowerCase() !== savedLang) ch_lang();

  // Lang menu
  $('#lang .dropdown-menu a').on("click tap", function()
    {
      ch_lang();
      ntnl($('#curr_lang').text());
      $.cookie("clang", $('#curr_lang').text().toLowerCase());
    });

  ntnl($('#curr_lang').text());

// ========================================================================
// Calculation by ratio
// ========================================================================

  function clclt(type)
  {
      if(!$("#chainstay").val()) $("#chainstay").val(440);
      if($("#chainstay").val() < 300) $("#chainstay").fadeOut(0, function() { $("#chainstay").val(300).fadeIn(500); });
      if(!$("#ratio").val()) $("#ratio").val(1.68);
      if($("#ratio").val() < 0.5) $("#ratio").fadeOut(0, function() { $("#ratio").val(0.5).fadeIn(500); });
      if(!$("#front_chring").val()) $("#front_chring").val(30);
      if($("#front_chring").val() < 30) $("#front_chring").fadeOut(0, function() { $("#front_chring").val(30).fadeIn(500); });
      if($("#front_chring").val() > 60) $("#front_chring").fadeOut(0, function() { $("#front_chring").val(60).fadeIn(500); });
      
      var n = 1;
      var count = 1;
      var n_html = '<div class="row danger">\
               <div>\
                 <lable title="Chain length">Нет совпадений</lable>\
              </div>\
            </div>';
      
    // === by ratio ===
      if(type == 'ratio')
      {
        $("#result").html('');
        $("#result").hide();
        
        while (n <=26)
        {
          var ln = 29+n; // Количество зубов передней звезды
          var sn = Math.round(ln/$("#ratio").val()); // Количество зубов задней звезды
          
          if(sn > 12) var result = clck_step(ln, sn);
            
          r_html = get_content(count, ln, sn, result, 'raw');
          m_html = get_content (count, ln, sn, result, 'modal');
          
          if(result['match'])
          {
            $("#result").append(r_html).show('slow');
            $("#modals_block").append(m_html);
            count++;
          }
          
          n++;
        }
      }
    // === by chainring ===
      else
      {
        $("#result_ch").html('');
        $("#result_ch").hide();
        
        // var ln = Math.round(sn*$("#ratio").val()); // Количество зубов передней звезды
        var ln = $('#front_chring').val();
        
        while(n < 30)
        {
          var sn = 11+n; // Количество зубов передней звезды
          
          var result = clck_step(ln, sn);
          
          r_html = get_content(count, ln, sn, result, 'raw');
          m_html = get_content (count, ln, sn, result, 'modal');

          if(result['match'])
          {
            $("#result_ch").append(r_html).show('slow');
            $("#modals_block").append(m_html);
            count++;
          }

          n++;
         }
        
      }
      
    // === rows selection ===
    
      $('#result .row, #result_ch .row').off("click");
      
      $('#result .row, #result_ch .row').click(function(e){
        // if (e.target !== this) return;
        if(e.target.tagName.toLowerCase() != 'div') return;
        if($(this).hasClass("highlight"))
            $(this).removeClass('highlight');
        else
            $(this).addClass('highlight');
      })
      
  };

// ========================================================================
// Functions 
// ========================================================================

// Even check
  function isEven(someNumber)
  {
    return (someNumber % 2 === 0) ? true : false;
  };

// Internationalization
  function ntnl(lang)
  {
    lang = lang.toLowerCase();
    $('*[ml_label]').each(function()
    {
      //if(labels.hasOwnProperty($(this).attr("ml_label")))
      {
        if(!$(this).val() && $(this).text()) $(this).text(labels[$(this).attr("ml_label")][lang]);
          else $(this).val(labels[$(this).attr("ml_label")][lang]);
      }
    })
  };

  function ch_lang()
  {
    var old_lang = $("#curr_lang").text();
    $("#curr_lang").text($('#lang .dropdown-menu a').text());
    $('#lang .dropdown-menu a').text(old_lang);
  }

// Ratio eval iteration
  function clck_step(ln, sn)
  {
      
      var result = new Array(4);
      result['match'] = false;
      if(ln <= sn) return result;
      
      rad_l = (180 / ln) * Math.PI / 180; // преобразование в радианы
      rad_s = (180 / sn) * Math.PI / 180; // преобразование в радианы
      
      // https://www.wolframalpha.com/input/?i=32-gon+edge%3D12.7mm
      lr = $("#pitch").val() / (2 * Math.sin(rad_l)); // радиус большой звезды
      result['large_radius'] = lr;
      // https://www.wolframalpha.com/input/?i=19-gon+edge%3D12.7mm
      sr = $("#pitch").val() / (2 * Math.sin(rad_s)); // радиус малой звезды
      result['small_radius'] = sr;
      
      // прямой учаток цепи
      c  = Math.sqrt( Math.pow($("#chainstay").val(),2) + Math.pow(lr-sr,2) );
      result['chain_straight'] = c;
      result['chains_dif'] = lr-sr;
      // общая длина цепи
      l = (ln / 2 * $("#pitch").val()) + (sn / 2 * $("#pitch").val()) + (c * 2);
      result['chain_length'] = l;
      // количество звеньев
      links = Math.round(l/$("#pitch").val());
      result['chain_links'] = links;
      // провисание цепи
      hit = (l/$("#pitch").val()) - Math.floor(l/$("#pitch").val());
    
      // проверка длины цепи и четности звеньев
      if(hit < 0.5 && isEven(links)) result['match'] = true;
    
    return result;
  }

  function get_content (n, ln, sn, result, type)
  {
    var r_html = '';
    var m_html = '';
    r_html += '<div class="row" id="tr_'+n+'">\
               <div class="col-xs-12">\
                 <div class="heading">\
                   <input type="button" class="btn btn-info btn-sm" data-toggle="collapse" data-target=".collapse_'+n+'" value="⇅&nbsp;'+n+'" />\
                   <span class="row-fluid" data-toggle="collapse" data-target=".collapse_'+n+'">'+ln+'x'+sn+'</span>\
                   <!-- <input type="button" class="btn btn-info btn-sm btn-check" data-toggle="modal" data-target="#Modal_'+n+'" value="?" /> -->\
                 </div>\
              </div>\
            </div>';
    r_html += '<div class="row collapse_'+n+' collapse" data-toggle="tooltip" title="диаметры звезд">\
               <div class="col-xs-6">\
                 <lable ml_label="ch_d">Диаметры звезд:</lable>\
              </div>\
              <div class="col-xs-6">\
                <input type="number" name="r_'+n+'_d_l" id="r_'+n+'_d_l" class="form-control" size="5" maxlength="5" value="'+(result['large_radius']*2).toFixed(1)+'" readonly>\
                <input type="number" name="r_'+n+'_d_s" id="r_'+n+'_d_s" class="form-control" size="5" maxlength="5" value="'+(result['small_radius']*2).toFixed(1)+'" readonly>\
              </div>\
            </div>';
    r_html += '<div class="row collapse_'+n+' collapse" data-toggle="tooltip">\
               <div class="col-xs-6">\
                 <lable ml_label="chain_straight">Длинна прямых участков цепи:</lable>\
              </div>\
              <div class="col-xs-6">\
                <input type="number" name="rw_'+n+'_sch_l" id="rw_'+n+'_sch_l" class="form-control" size="5" maxlength="5" value="'+(result['chain_straight']).toFixed(1)+'" readonly>\
              </div>\
            </div>';
    r_html += '<div class="row collapse_'+n+' collapse" data-toggle="tooltip" title="длина цепи">\
               <div class="col-xs-6">\
                 <lable ml_label="chain_length">Общая длина цепи:</lable>\
              </div>\
              <div class="col-xs-6">\
                <input type="number" name="r_w'+n+'_ch_l" id="rw_'+n+'_ch_l" class="form-control" size="5" maxlength="5" value="'+(result['chain_length']).toFixed(2)+'" readonly>\
              </div>\
            </div>';
    r_html += '<div class="row collapse_'+n+' collapse" data-toggle="tooltip" title="количество звеньев">\
               <div class="col-xs-6">\
                 <lable ml_label="chain_links">Количество звеньев цепи:</lable>\
              </div>\
              <div class="col-xs-6">\
                <input type="number" name="r_w'+n+'_ch_lnks" id="rw_'+n+'_chl_lnks" class="form-control" size="5" maxlength="5" value="'+result['chain_links']+'" readonly>\
              </div>\
            </div>';
    r_html += '<div class="row collapse_'+n+' collapse" data-toggle="tooltip" title="Передаточное отношение">\
               <div class="col-xs-6">\
                 <lable ml_label="ch_ratio">Передаточное отношение:</lable>\
              </div>\
              <div class="col-xs-6">\
                <input type="number" name="r_w'+n+'_ch_ratio" id="rw_'+n+'_ratio" class="form-control" size="5" maxlength="5" value="'+(ln/sn).toFixed(2)+'" readonly>\
              </div>\
            </div>';
    /*
    r_html += '<div class="row collapse_'+n+' collapse">\
               <div class="col-xs-12" id="svg_'+n+'">\
                  <!-- Width: '+(Number(result['large_radius'])+result['small_radius']+Number($("#chainstay").val())*1.1)+' -->\
                  <!-- Height: '+(result['large_radius']*2.1)+' -->\
                  <svg width="100%" height="'+(result['large_radius']*2.1)+'">\
                     <circle cx="'+result['large_radius']+'" cy="'+(result['large_radius']*2.1)/2+'" r="'+result['large_radius']+'" stroke="gray" stroke-width="1" fill="silver" />\
                     <!-- <circle cx="'+(result["large_radius"]+Number($("#chainstay").val())+result["small_radius"]/2)+'" cy="'+(result["large_radius"]*2.1)/2+'" r="'+result["small_radius"]+'" stroke="gray" stroke-width="1" fill="silver" /> -->\
                     <circle cx="'+result['large_radius']+'" cy="'+(result["large_radius"]*2.1)/2+'" r="'+result["small_radius"]+'" stroke="gray" stroke-width="1" fill="silver" />\
                     Sorry, your browser does not support inline SVG.\
                  </svg>\
              </div>\
            </div>';
    */
    
    if(type == 'modal')
    {
          m_html += '<div id="Modal_'+n+'" class="modal fade" role="dialog">\
              <div class="modal-dialog">\
                <div class="modal-content">\
                  <div class="modal-header">\
                    <button type="button" class="close" data-dismiss="modal">&times;</button>\
                    <h4 class="modal-title">Ссылки на расчеты</h4>\
                  </div>\
                  <div class="modal-body">\
                    <ul>\
                      <li><a href="https://www.wolframalpha.com/input/?i=round('+ln+'%2F'+$("#ratio").val()+')" target="_blank">Размер задней звезды</a></li>\
                      <li><a href="https://www.wolframalpha.com/input/?i='+ln+'-gon+edge%3D12.7mm" target="_blank">Диаметр передней звезды</a></li>\
                      <li><a href="https://www.wolframalpha.com/input/?i='+sn+'-gon+edge%3D12.7mm" target="_blank">Диаметр задней звезды</a></li>\
                      <li><a href="https://www.wolframalpha.com/input/?i=sqrt('+$("#chainstay").val()+'%5E2%2B('+result['large_radius']+'-'+result['small_radius']+')%5E2)" target="_blank">Длина прямого участка</a></li>\
                      <li><a href="https://www.wolframalpha.com/input/?i=round('+result['chain_length']+'%2F12.7)" target="_blank">Количество звеньев</a></li>\
                    </ul>\
                  </div>\
                  <!-- <div class="modal-footer">\
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\
                  </div> -->\
                </div>\
              </div>\
            </div>';
      return m_html;
    }
    else return r_html;
    
    
  }
